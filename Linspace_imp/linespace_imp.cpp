#include <iostream>
#include <math.h>
#include <stdexcept>
#include <torch/torch.h>

/*
 *  Linspace op implementation using combination of different ops
 *  Functionality-Creates a one-dimensional tensor of size steps whose
 *  values are evenly spaced from start to end, inclusive.
 *  This file include linspace implemetation and test cases
 *  Other ops used includes -sub,arange,unsqueeze,cat
 */

typedef torch::Tensor TORCH;

/*
 *   This linespace function custom_fmod include implementation of linspace op.
 *   It takes three arguments Start ,end and steps as a input and
 *   return a tensor of size steps.Takes inputs as a int or float type
 *
 */

template <typename T, typename S> TORCH linespace(T start, T end, S steps) {
  TORCH result2, t1, t2, temp1, stepval, new_stepval, new1_end;
  float new_start, new_end;
  int type;
  if (typeid(type) != typeid(steps)) {                                   /*CHecking for float step value*/
    throw std::invalid_argument("Step value cannot be of type float");
  }
  if (typeid(new_start) !=typeid(start)) {          /*If input[start and end] is not float then typecasting to a float type*/                        
    new_start = static_cast<float>(start);
    new_end = static_cast<float>(end);
  } else {
    new_start = start;
    new_end = end;
  }
  if (steps == 1) {                                      /*If step value is 1 then result will contain only start value*/
    result2 = torch::tensor(new_start);
  } else if (steps < 0) {
    throw std::invalid_argument("Step value should be negative");
  } else if (steps == 0) {
    throw std::invalid_argument("Step value should be non zero");
  } else if (fabs(start - end) <0.01f) {                /*Start and end same  then result tenor contains start value of size step*/
    result2 = torch::repeat_interleave(torch::tensor(new_start), steps);                         
  } else {
    t1 = torch::sub(torch::tensor(new_end), torch::tensor(new_start));
    t2 = torch::sub(torch::tensor(steps), torch::tensor(1));
    stepval = torch::unsqueeze(torch::div(t1, t2), 0);
    temp1 = torch::arange(new_start, new_end, stepval[0].item<float>());
    new1_end = torch::unsqueeze(torch::tensor(new_end), 0); /*To change dim=1 */
    result2 = torch::cat({temp1, new1_end}, 0);
  }

  return result2;
}

/*  Test case implementation for linspace
 *  It includes total of 9 test cases
 */

/*Restricting to check equality of floating point result upto 4 precisions*/
TORCH rounding(TORCH input_tensor) {
  return (input_tensor * (float)pow(10, 4)).round() / (float)pow(10, 4);
}

const std::string check(TORCH res1,TORCH res2) {       /* This functions checks the equality of two resultant tensors*/
                                                          
  if (torch::equal(rounding(res1), rounding(res2)))
    return "Passed";
  else
    return "Failed";
}

int main() {
  TORCH result1, result2;

  /*
   *  This test case includes checking for
   *  1.Basic test cases for integer and float start and stop
   *  2.Equal Start and stop
   */

  float start1[] = {12, -1.8, -143.53, 8.9009, 10},
        end[] = {20, 2.8, -125.63, 10.1089, 10};
  int steps[] = {3, 20, 100, 1000, 9};
  for (int i = 0; i < 5; i++) {
    std::cout << "case" << i + 1 << ":\t";
    result2 = linespace<float, int>(start1[i], end[i], steps[i]);
    result1 = torch::linspace(start1[i], end[i], steps[i]);
    std::cout << check(result1, result2) << std::endl;
  }
  std::cout << "case6" << ":\t";
  result2 = linespace<int, int>(start1[0], end[0], steps[0]);
  result1 = torch::linspace(start1[0], end[0], steps[0]);
  std::cout << check(result1, result2) << std::endl;

  /*
   * Following test cases includes
   *  1.step value zero
   *  2.Step value less than zero
   *  3.step value as a float
   *  all these cases should raise exceptions
   */

  try {
    linespace<float, int>(12, 3, 0);
    std::cout << "case7:\tFailed" << std::endl;
  } catch (...) {
    std::cout << "case7:\tPassed" << std::endl;
  }
  try {
    linespace<float, int>(12, 3, -1);
    std::cout << "case8:\tFailed" << std::endl;
  } catch (...) {
    std::cout << "case8:\tPassed" << std::endl;
  }
  try {
    linespace<float, float>(12, 3, 0.7);
    std::cout << "case9:\tFailed" << std::endl;
  } catch (...) {
    std::cout << "case9:\tPassed" << std::endl;
  }
  return 0;
}
