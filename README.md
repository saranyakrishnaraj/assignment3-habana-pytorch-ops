This Repo contains implementation of linspace and fmod op implementation using other ops in C++ using PyTorch C++ Backend.

Functionality:

Linspace-Creates a one-dimensional tensor of size steps whose values are evenly spaced from start to end, inclusive.

fmod-Computes the element-wise remainder of division

It has two folder each contains cpp file for implemenation and testing and a CMakeLists.txt

##How to set it up##

Requirements:

1.Pytorch-This program is tested with Pytorch LTS version

2.LIbtorch Library-https://pytorch.org/cppdocs/installing.html

Once the above requirements met run the CMakeLists.txt to get the executables
