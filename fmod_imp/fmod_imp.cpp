#include <iostream>
#include <stdexcept>
#include <torch/torch.h>

/*
 *  Fmod op implementation using combination of different ops
 *  Functionality-Computes the element-wise remainder of division
 *  This file include fmod implemetation and test cases 
 *  Formula used to find the elementwise fmod: remainder=dividend-floor(dividend/divisor)*divisor
 *  Other ops used includes -sub,abs,mul,div,sign
 */

typedef torch::Tensor TORCH;               

/*
 *   This custom_fmod function custom_fmod include implementation of fmod op.
 *   It takes two argument dividend and divisor as a input and 
 *   return a tensor.Takes Dividend and divisor as a tensor of type
 *   int or float.
 */
 
TORCH custom_fmod(TORCH dividend,TORCH divisor){

TORCH new_divisor,signed_value,Positive_dividend,temp1,temp2,temp3,result2;
new_divisor=torch::abs(divisor);                     /*For handling negative divisor cases*/                 
signed_value=torch::sign(dividend);                 
Positive_dividend=torch::abs(dividend);              
temp1=torch::div(Positive_dividend,new_divisor);     
torch::floor_(temp1);                                               
temp2=torch::mul(temp1,new_divisor);                                      
temp3=torch::sub(Positive_dividend,temp2);
result2=torch::mul(temp3,signed_value);             /*making remainder having same sign as dividend*/                     
return result2;
}

/*
 *  This is a overloaded function of above custom_fmod to receive
 *  divisor input as a scalar,since fmod support both scalar and tensor 
 *  divisors.It also takes up both int and float type divisor
 *  This function includes checking for divisionbyzero error when divisor is zero
 *  for float dividend or divisor.
 */ 

template<typename T>
TORCH custom_fmod(TORCH dividend,T divisor){
  if((!torch::is_floating_point(dividend))&&(divisor==0)){ /* Typechecking whether the divisor is zero for integer divisor */
   throw std::runtime_error("Division by zero error\n");
  }
  TORCH x=torch::tensor(divisor);                         /*converting scalar divisor into tensor*/
  return custom_fmod(dividend,x);
}

/*
 *  Below code includes Test case implementation for fmod
 *  It includes total of 10 test cases
 */

/*
 * This rounding function purpose is to Restrict the checking
 * the equality of floating point result upto 4 precisions
 */
TORCH rounding(TORCH input_tensor){                                
    return (input_tensor * pow(10,4)).round() / pow(10,4);
}


const std::string 
check(TORCH res1,TORCH res2){                 /* This functions checks the equality of two resultant tensors*/

   if (torch::equal(rounding(res1),rounding(res2)))
   return "Passed";
 
 return "Failed";
}

/*  This Testcases function takes up float or int divisor
 *  It computes the passes the given input to original fmod op and custom fmod op
 *  and compare the correctness of these results.
 */
template<typename T>
void Testcases(TORCH dividend,T * divisor,int arrsize){
  TORCH result1,result2;
  for(int i=0;i<arrsize;i++)
  {
    if(typeid(i)!=typeid(divisor[i])){    //for float divisor
      std::cout<<"case"<<i+1<<":\t";
    result2=custom_fmod<float>(dividend,divisor[i]); //custom fmod implementation
    }
    else{
      std::cout<<"case"<<i+1+4<<":\t";            //for integer divisor
    result2=custom_fmod<int>(dividend,divisor[i]);
    }
    result1=torch::fmod(dividend,divisor[i]); //Original fmod implementation
    
    std::cout<<check(result1,result2)<<std::endl; 
    
  }
}

int main()
{
  TORCH dividend;
  /*
   *Following test case includes:
   *Divisor as a scalar
   *Divisor as a tensor
   *Integer and float dividend and divisor
   */

  dividend=torch::tensor({-12.55,333.9,0.0});                             
  float divisor[]={12.5,0.8888,-999999.0,12056.15000};               /*divisor as scalar*/
  std::cout<<"Fmod implementation for float divisor"<<std::endl;
  Testcases<float>(dividend,divisor,4);
  std::cout<<"Fmod implementation for integer divisor"<<std::endl;
  dividend=torch::tensor({-12555,333,597032,-1});
  int divisor1[]={-9,90,-51234,10000000};
  Testcases<int>(dividend,divisor1,3);

  dividend=torch::tensor({1255});
  TORCH divi=torch::tensor({15.5});        /*divisor as tensor*/
  TORCH r1=custom_fmod(dividend,divi);
  TORCH r2=torch::fmod(dividend,divi); 
  std::cout<<"case8:\t";
  std::cout<<check(r1,r2)<<std::endl; 

  /*
   * This test case includes
   * Divisor value zero case for integer dividend
   * It will raise a runtime error
   */

  try {
      custom_fmod<int>(dividend,0);
      std::cout<<"case9:\tFailed"<<std::endl;
    }catch(std::runtime_error& e) {
        std::cout<<"case9:\tPassed"<<std::endl;
    } 

  /*
   * This test case includes
   *  Divisor value zero case for float dividend
   *  It will return value as nan
   */
    dividend=torch::tensor({1255.5});
    if(torch::isnan(custom_fmod<float>(dividend,0)).any().item<bool>())
     std::cout<<"case10:\tPassed"<<std::endl;
    else
     std::cout<<"case10:\tFailed"<<std::endl;
   
  return 0;
}
